package mataDra.view.scenes;

public abstract class Scene {
    public enum SceneType {
        opening, making, scenario, battle, dead, gameOver, field, town,ending
    }

    private int index;
    private String name;
    private SceneType sceneType;



    /**
     * @param index
     * @param name
     * @param sceneType
     */
    public Scene(int index, String name, SceneType sceneType) {
        this.index = index;
        this.name = name;
        this.sceneType = sceneType;
    }



    public abstract void change(SceneType sceneType, int index);

    // ゲッターセッター自動生成
    public int getIndex() {
        return index;
    }



    public void setIndex(int index) {
        this.index = index;
    }



    public String getName() {
        return name;
    }



    public void setName(String name) {
        this.name = name;
    }



    public SceneType getSceneType() {
        return sceneType;
    }



    public void setSceneType(SceneType sceneType) {
        this.sceneType = sceneType;
    }



}
