package mataDra.entity.creatures;

import java.util.LinkedList;
import java.util.List;

import mataDra.entity.Entity;
import mataDra.entity.ability.AbilityEntity;

public abstract class CreatureEntity extends Entity implements Cloneable{

	// 行動可能、行動不能、死亡
    public enum BattleStateType {
        MOVABLE, INMOVABLE, DEAD
    };

   //攻撃属性、炎、水、木、闇、光
    public enum Attribute{
    	FIRE,WATER,WOOD,DARKNESS,LIGHT
    }


    private String name;
    private int hp;// 体力
    private int charge;// スキルチャージ量
    private int attack;
    private int experience;// 経験値
    private int level;// 設定レベル
    private List<AbilityEntity> abilities= new LinkedList<>();//使用可能スキル
    private BattleStateType battleStateType; // 戦闘可能状態
    private Attribute attribute;//攻撃属性



    /**コンストラクタ自動生成
     * @param index
     * @param name
     * @param hp
     * @param charge
     * @param attack
     * @param experience
     * @param level
     * @param abilities
     * @param battleStateType
     * @param attribute
     */
    public CreatureEntity(int index, String name, int hp, int charge, int attack, int experience, int level,
			List<AbilityEntity> abilities, BattleStateType battleStateType, Attribute attribute) {
		super(index);
		this.name = name;
		this.hp = hp;
		this.charge = charge;
		this.attack = attack;
		this.experience = experience;
		this.level = level;
		this.abilities = abilities;
		this.battleStateType = battleStateType;
		this.attribute = attribute;
	}



	/* オブジェクトをディープコピーするためにcloneメソッドをオーバーライド
	 * @see java.lang.Object#clone()
	 */
	@Override
	public CreatureEntity clone() {
		// TODO 自動生成されたメソッド・スタブ

			try {
				return (CreatureEntity)super.clone();
			} catch (CloneNotSupportedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
				return null;
			}



	}

//ゲッターセッター自動生成

	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public int getHp() {
		return hp;
	}



	public void setHp(int hp) {
		this.hp = hp;
	}



	public int getCharge() {
		return charge;
	}



	public void setCharge(int charge) {
		this.charge = charge;
	}



	public int getAttack() {
		return attack;
	}



	public void setAttack(int attack) {
		this.attack = attack;
	}



	public int getExperience() {
		return experience;
	}



	public void setExperience(int experience) {
		this.experience = experience;
	}



	public int getLevel() {
		return level;
	}



	public void setLevel(int level) {
		this.level = level;
	}



	public List<AbilityEntity> getAbilities() {
		return abilities;
	}



	public void setAbilities(List<AbilityEntity> abilities) {
		this.abilities = abilities;
	}



	public BattleStateType getBattleStateType() {
		return battleStateType;
	}



	public void setBattleStateType(BattleStateType battleStateType) {
		this.battleStateType = battleStateType;
	}



	public Attribute getAttribute() {
		return attribute;
	}



	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}









}