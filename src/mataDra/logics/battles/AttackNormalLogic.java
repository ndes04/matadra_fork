package mataDra.logics.battles;

import java.util.List;

import assets.items.equip.Armor;
import assets.items.equip.Weapon;
import assets.items.equip.Weapon.WeaponType;
import mataDra.entity.creatures.Character;
import mataDra.view.ui.MessageArea;
import mataDra.viewss.ui.ImagePlayer;
import mataDra.viewss.ui.SoundPlayer;

/**
 * ノーマル攻撃
 *
 *
 */
public class AttackNormalLogic extends AttackLogic {

    /**
     * 単体攻撃設定
     *
     * @param offenceCharacter
     * @param abilityIndex
     * @param defenceCharacter
     * @param partyIndex
     */
    public void setAttack(Character oc, int abilityIndex, List<Character> dcs) {

        Character dc;// ディフェンスキャラクター
        String msg = "";// メッセージ
        int bonus;// ステータスボーナス
        int point;// 攻撃力
        WeaponEntity weapon;// 武器クラス
        Armor armor;// 防具クラス
        double damping;//攻撃減衰率
        int attack;// 総合攻撃力
        int defence;// 総合防御力
        int hit;// 命中率
        boolean isMiss;// 命中判定
        int damage;// 最終ダメージ
        int critical;
        boolean isCritical;
        // ディフェンスパーティの数だけ繰り返す。
        for (int i = 0; i < dcs.size(); i++) {
            dc = dcs.get(i);
            weapon = oc.getWeapon();
            armor = dc.getArmor();
            bonus = getBonus(oc.getTechnique());
            point = weapon.getPoint();

            // ブーメラン系武器の場合、攻撃力減衰、それ以外は減衰なしの三項演算子。
            damping = weapon.getWeaponType().equals(WeaponType.throwing) ? calcDampingRate(dcs.size()) : 1;
            attack = (int) (calcTotalAttack(oc.getStrength(), bonus, point) * damping);
            defence = calcTotalDefence(dc.getVitality(), armor.getPoint());
            hit = calcAttackHitRate(oc.getAgility(), dc.getAgility());
            isMiss = isAttackMiss(hit);
            critical = calcCriticalRate(oc.getTechnique(), bonus, dc.getTechnique());
            isCritical = isAttackCritical(critical);
            damage = calcTotalDamage(attack, defence);

            // 攻撃開始
            msg = oc.getName() + "のこうげき";
            MessageArea.message(msg);
            ImagePlayer.play(weapon.getImage());
            SoundPlayer.play(weapon.getSound());
            // クリティカル判定
            if (isCritical) {
                CriticalDamageMessage(oc, dc);
            } else {
                AttackDamageMessage(dc, damage, isMiss);
            }
            deadMessage(dc);
        }
    }
}
